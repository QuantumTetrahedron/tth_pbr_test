//
// Created by bartek on 03.02.2020.
//

#include <TTH/Input/Input.h>
#include "Rotator.h"

void Rotator::Start() {

}

void Rotator::Update(float dt) {
    float xr = TTH::Input::GetAxis("Xrotation");
    float yr = TTH::Input::GetAxis("Yrotation");
    float zr = TTH::Input::GetAxis("Zrotation");

    auto p = parent.lock();
    p->transform.rotation = p->transform.rotation + glm::vec3(xr, yr, zr);
}

void Rotator::OnLeave() {

}
