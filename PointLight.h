//
// Created by bartek on 07.01.2020.
//

#ifndef THETETRAHEDRONTEST_POINTLIGHT_H
#define THETETRAHEDRONTEST_POINTLIGHT_H


#include <TTH/Graphics/LightComponent.h>

class PointLight : public TTH::LightComponent {
public:
    void SetUniforms(const TTH::Shader &shader) override;

    bool CastsShadows() override;

    void CreateShadowMap() override;

    glm::vec3 color;
    std::string name;

protected:
    bool LoadFromFile(const TTH::IniFile &file) override;

    TTH_COMPONENT(PointLight)
};


#endif //THETETRAHEDRONTEST_POINTLIGHT_H
