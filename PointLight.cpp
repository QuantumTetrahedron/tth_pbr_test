//
// Created by bartek on 07.01.2020.
//

#include "PointLight.h"
#include <TTH/Resources/Shader.h>

void PointLight::SetUniforms(const TTH::Shader &shader) {
    shader.SetVec3(name + ".position", GetParent()->transform.position);
    shader.SetVec3(name + ".color", color);
}

bool PointLight::CastsShadows() {
    return false;
}

void PointLight::CreateShadowMap() {

}

bool PointLight::LoadFromFile(const TTH::IniFile &file) {
    file.RequireValue("name", name);
    file.RequireValue("color", color);
    return true;
}
