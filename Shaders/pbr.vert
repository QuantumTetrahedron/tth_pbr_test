#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTangent;
layout(location = 4) in vec3 vBitangent;

uniform mat4 model;

layout (std140) uniform Matrices{
    mat4 projection;
    mat4 view;
    mat4 projectionView;
};

struct Light{
    vec3 position;
    vec3 color;
};

uniform Light pointLight;
uniform Light pointLight2;
uniform vec3 viewPos;

out Light PointLight;
out Light PointLight2;
out vec3 ViewPos;

out vec3 FragPos;
out vec2 TexCoords;

void main() {
    FragPos = vec3(model * vec4(vPosition, 1.0));
    TexCoords = vTexCoords;

    mat3 normalMatrix = transpose(inverse(mat3(model)));
    vec3 T = normalize(normalMatrix * vTangent);
    vec3 N = normalize(normalMatrix * vNormal);
    T = normalize(T - dot(T, N) * N);
    //vec3 B = cross(N, T);
    vec3 B = normalize(normalMatrix * vBitangent);

    if (dot(cross(N, T), B) < 0.0){
        T = T * -1.0;
    }

    mat3 TBN = transpose(mat3(T,B,N));
    FragPos = TBN * FragPos;
    PointLight.position = TBN * pointLight.position;
    PointLight.color = pointLight.color;
    PointLight2.position = TBN * pointLight2.position;
    PointLight2.color = pointLight2.color;
    ViewPos = TBN * viewPos;

    gl_Position = projectionView * model * vec4(vPosition, 1.0);
}
