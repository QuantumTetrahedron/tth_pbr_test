#version 330 core

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexCoords;
layout(location = 2) in vec3 vNormal;

uniform mat4 model;

layout (std140) uniform Matrices{
    mat4 projection;
    mat4 view;
    mat4 projectionView;
};
//uniform mat4 projectionView;

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;

void main() {
    FragPos = vec3(model * vec4(vPosition, 1.0));
    TexCoords = vTexCoords;
    Normal = normalize(transpose(inverse(mat3(model))) * vNormal);
    gl_Position = projectionView * model * vec4(vPosition, 1.0);
}
