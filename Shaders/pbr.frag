#version 330 core

out vec4 fragColor;

struct Light{
    vec3 position;
    vec3 color;
};

in vec3 FragPos;
in vec2 TexCoords;

in Light PointLight;
in Light PointLight2;
in vec3 ViewPos;

uniform vec3 colorTint;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;
uniform sampler2D texture_height1;

const float PI = 3.14159265359;

float DistributionGGX(vec3 N, vec3 H, float roughness){
    float a = roughness * roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH * NdotH;

    float num = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness){
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0; //2

    float num = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness){
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float GGX2 = GeometrySchlickGGX(NdotV, roughness);
    float GGX1 = GeometrySchlickGGX(NdotL, roughness);

    return GGX1 * GGX2;
}

vec3 FresnelSchlick(float cosTheta, vec3 F0){
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 CalcLight(Light pointLight, vec3 N, vec3 V, vec3 F0, vec3 albedo, float roughness, float metallic){
    vec3 L = normalize(pointLight.position - FragPos);
    vec3 H = normalize(V + L);

    float distance = length(pointLight.position - FragPos);
    float attenuation = 1.0 / (distance * distance);
    vec3 radiance = pointLight.color * attenuation;

    // cook-torrance
    float NDF = DistributionGGX(N, H, roughness);
    float G = GeometrySmith(N, V, L, roughness);
    vec3 F = FresnelSchlick(max(dot(H,V), 0.0), F0);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - metallic;

    vec3 numerator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = numerator / max(denominator, 0.001);

    float NdotL = max(dot(N, L), 0.0);
    return (kD * albedo / PI + specular) * radiance * NdotL;
}

void main() {
    vec3 albedo = texture(texture_diffuse1, TexCoords).rgb;
    float metallic = texture(texture_specular1, TexCoords).r;
    float roughness = texture(texture_height1, TexCoords).r;

    vec3 N = normalize(texture( texture_normal1, TexCoords ).rgb);
    N = normalize(N*2.0-1.0);

    vec3 V = normalize(ViewPos - FragPos);

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    vec3 Lo = vec3(0.0);
    Lo += CalcLight(PointLight, N, V, F0, albedo, roughness, metallic);
    Lo += CalcLight(PointLight2, N, V, F0, albedo, roughness, metallic);

    vec3 ambient = 0.03 * albedo; // * ao_texture
    vec3 color = ambient + Lo;

    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));

    fragColor = vec4(color, 1.0);
}
