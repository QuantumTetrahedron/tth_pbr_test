//
// Created by bartek on 03.02.2020.
//

#ifndef TTH_PBR_ROTATOR_H
#define TTH_PBR_ROTATOR_H


#include <TTH/Core/BehaviourComponent.h>

class Rotator : public TTH::BehaviourComponent{
public:
    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

private:
    TTH_COMPONENT(Rotator)
};


#endif //TTH_PBR_ROTATOR_H
