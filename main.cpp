#include <iostream>

#include <TTH/Core/Engine.h>
#include <TTH/Resources/ResourceManager.h>

int main() {

    TTH::Initializer initializer;
    initializer.rendering.depthTesting = true;
    initializer.rendering.blending = true;

    initializer.game.title = "PBR test";
    initializer.game.startScene = "main";
    initializer.game.mouseEnabled = false;
    initializer.game.windowWidth = 1600;
    initializer.game.windowHeight = 900;

    TTH::Game::Initialize(initializer);

    TTH::Input::AddAxis("Horizontal", {TTH::Key::D, TTH::Key::Right}, {TTH::Key::A, TTH::Key::Left});
    TTH::Input::AddAxis("Vertical", {TTH::Key::W, TTH::Key::Up}, {TTH::Key::S, TTH::Key::Down});

    TTH::Input::AddAxis("Xrotation", {TTH::Key::R}, {TTH::Key::F});
    TTH::Input::AddAxis("Yrotation", {TTH::Key::T}, {TTH::Key::G});
    TTH::Input::AddAxis("Zrotation", {TTH::Key::Y}, {TTH::Key::H});

    TTH::ResourceManager::LoadFromFile("GameData/ResourceOptions.ini");

    TTH::Game::Start();

    TTH::Game::MainLoop();

    TTH::Game::Shutdown();

    return 0;
}