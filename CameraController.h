
#ifndef THETETRAHEDRON_CAMERACONTROLLER_H
#define THETETRAHEDRON_CAMERACONTROLLER_H

#include "TTH/Core/BehaviourComponent.h"
#include "TTH/Core/ComponentFactory.h"
#include "TTH/Core/CameraComponent.h"

class CameraController : public TTH::BehaviourComponent{
public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

private:
    bool LoadFromFile(const TTH::IniFile& file) override;

    std::shared_ptr<TTH::CameraComponent> cam;

    float speed;
    float mouseSensitivity;
    bool negativeEffect;

    TTH_COMPONENT(CameraController)
};


#endif //THETETRAHEDRON_CAMERACONTROLLER_H
