
#include "CameraController.h"
#include "TTH/Core/Object.h"
#include "TTH/Input/Input.h"
#include "TTH/Utils/IniFile.h"

void CameraController::Start() {
    cam = GetParent()->GetComponent<TTH::CameraComponent>();
    negativeEffect = false;
}

void CameraController::Update(float dt) {
    glm::vec2 offset = TTH::Input::GetMouseOffset();

    auto par = parent.lock();

    float& yaw = par->transform.rotation.y;
    float& pitch = par->transform.rotation.x;

    yaw += offset.x * mouseSensitivity;
    pitch += offset.y * mouseSensitivity;

    if(pitch > 89.0f){
        pitch = 89.0f;
    } else if(pitch < -89.0f){
        pitch = -89.0f;
    }

    yaw = glm::mod(yaw,360.0f);

    glm::vec3 front;
    float p = glm::radians(pitch);
    float y = glm::radians(yaw);
    front.x = glm::cos(p) * glm::cos(y);
    front.y = glm::sin(p);
    front.z = glm::cos(p) * glm::sin(y);
    front = glm::normalize(front);

    cam->SetFront(front);

    glm::vec3 right = glm::cross(front, glm::vec3(0.0f,1.0f,0.0f));
    right = glm::normalize(right);

    glm::vec3 up = glm::cross(right, front);
    up = glm::normalize(up);

    float forwardMove = TTH::Input::GetAxis("Vertical");
    float rightMove = TTH::Input::GetAxis("Horizontal");

    par->transform.position += forwardMove * front * dt * speed;
    par->transform.position += rightMove * right * dt * speed;

    if(TTH::Input::GetButton(TTH::Key::Q)){
        if(negativeEffect){
            negativeEffect = false;
            cam->RemovePostprocessEffect("edgeDetect");
        } else {
            negativeEffect = true;
            cam->AddPostprocessEffect("edgeDetect");
        }
    }
    /*
    if(TTH::Input::GetButton(TTH::Key::Q) < 1 && negativeEffect){

    }*/
}

void CameraController::OnLeave() {

}

bool CameraController::LoadFromFile(const TTH::IniFile& file) {
    if(!file.GetValue("speed", speed)){
        std::cerr << "Missing speed parameter from CameraController" << std::endl;
        return false;
    }

    if(!file.GetValue("mouseSensitivity", mouseSensitivity)){
        std::cerr << "Missing mouseSensitivity parameter from CameraController" << std::endl;
        return false;
    }

    return true;
}